use log::info;

// This function works on *any* TCP stack (plus some custom stuff), including embedded ones -- only main() is what makes
// this use POSIX sockets.
//
// There *is* still a bit of a std dependency in that the code can't know when the next request is
// here, so right now it's using std::thread::sleep to stay idle, but that's not realted to how
// coap-message us used here.
fn run<S>(stack: &mut S)
where
    S: embedded_nal::TcpFullStack + embedded_nal_tcpextensions::TcpExactStack,
{
    let mut sock = stack.socket().expect("Can't create a socket");

    let log = Some(coap_message_demos::log::Log::start_once());

    let mut handler = coap_message_demos::full_application_tree(log);

    stack.bind(&mut sock, 5683).expect("Can't bind to port");
    info!("Server is ready.");

    let mut pool = embedded_nal_minimal_coaptcpserver::ServerPool::<S, 4, 1152>::new(sock);

    loop {
        pool.poll(stack, &mut handler)
            .expect("Actual error in polling (accepting?)");
        // See <https://github.com/rust-embedded-community/embedded-nal/issues/47>
        std::thread::sleep(std::time::Duration::from_millis(50));
    }
}

fn main() {
    let mut stack = std_embedded_nal::Stack::default();

    // If we used any other than the default stack (with its embedded-nal-tcpextensions feature)
    // let mut stack = embedded_nal_tcpextensions::BufferedStack::<_, 2000>::new(stack);

    run(&mut stack);
}
