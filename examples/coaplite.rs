use coap_lite::{CoapRequest, Packet};
use std::net::UdpSocket;

use coap_message::error::RenderableOnMinimal;
use coap_handler::Handler;

fn main() {
    let log = Some(coap_message_demos::log::Log::start_once());

    let mut handler = coap_message_demos::full_application_tree(log);

    let socket = UdpSocket::bind("localhost:5683").unwrap();
    let mut buf = [0; 1280];

    loop {
        let (size, src) = socket.recv_from(&mut buf).expect("Didn't receive data");

        let packet = Packet::from_bytes(&buf[..size]).unwrap();
        let request = CoapRequest::from_packet(packet, src);

        let extracted = handler.extract_request_data(&request.message);

        let mut response = request.response.unwrap();
        match extracted {
            Ok(extracted) => {
                if let Err(e2) =
                    handler.build_response(&mut response.message, extracted)
                {
                    response.message.payload = Default::default();
                    response.message.clear_all_options();
                    e2.render(&mut response.message).unwrap();
                }
            }
            Err(e) => {
                e.render(&mut response.message).unwrap();
            }
        }

        let packet = response.message.to_bytes().unwrap();
        socket
            .send_to(&packet[..], &src)
            .expect("Could not send the data");
    }
}
