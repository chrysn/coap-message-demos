[![Build Status](https://gitlab.com/chrysn/coap-message-demos/badges/master/pipeline.svg)](https://gitlab.com/chrysn/coap-message-demos/commits/master)
![Maintenance](https://img.shields.io/badge/maintenance-experimental-blue.svg)

# coap-message-demos

This crate contains demo applications for CoAP on Rust

All demos use the ecosystem around the [coap-message] crate. They come in two variations:

* "applications" contain code that would typically be the high-level code that executes
  business logic.

  They are a mix of standalone resource implementations, collections thereof into a
  whole-server handler, and possibly client code.

  They reside in the `src/` directory, and are available as library modules. This allows
  integrating them into other demo code, eg. into examples of a coap-message implementation.

* "examples" are the stand-alone executable binaries using various backends.

  They pick suitable applications, and wrap them with a CoAP implementation of choice into a
  program that can be run with `cargo run --example X`.

  Currently, the examples in this crate show the use of:

  * [coap-lite](https://crates.io/crates/coap-lite),
    a building block for CoAP-over-UDP libraries, running directly on a socket in the example.

  * [the coap crate](https://crates.io/crates/coap),
    which provides a full implementation, and can interface with coap-message by virtue of
    using coap-lite as a backend.

  * [embedded-nal-minimal-coapserver](https://crates.io/crates/embedded-nal-minimal-coapserver),
    which implements CoAP-over-UDP on the Embedded Network Abstraction Layer
    and processes messages through the [coap_handler] types.
    For the example, it uses a std implementation of embedded-nal.

  Examples that need larger ecosystem support and can not simply be launched natively by `cargo
  run --example` are not included here, but show (maybe even better) what the coap-message
  ecosystem is capable of providing:

  * [verdigris](https://crates.io/crates/verdigris)
    is an implementation of CoAP that runs in the web browser and uses CoAP-over-WebSockets.
    It includes the demo applications in its color server sub-application, where they can be
    accessed through a proxying Resource Directory.

  * [RIOT](https://riot-os.org/) is an embedded operating system for the Internet of Things.
    In its [rust-gcoap example](https://github.com/RIOT-OS/RIOT/tree/master/examples/rust-gcoap),
    the application runs the no_std part of the demo applications on RIOT's own gcoap
    implementation.

Usage
-----

The examples are all configured to run a selection of the applications; which they are depends
on the selected features.

For minimal operation, run the examples as

```sh
$ cargo run --example EXNAME --features example-EXNAME
```

where `EXNAME` is substituted with any of the examples -- currently `coaplite`, `coap_crate` or
`std_embedded_nal_minicoapserver`.

To explore all features, just run with

```sh
$ cargo run --example EXNAME --all-features
```

which, for example, adds access to a system [::log].

All the same can be accessed, for example, by using [aiocoap-client]:

```sh
$ aiocoap-client coap://localhost/.well-known/core
# application/link-format content was re-formatted
<>; ct=0; title="Landing page",
</time>; ct=0; title=Clock,
</poem>; sz=1338,
</cbor/1>; ct=60,
</cbor/2>; ct=60,
</cbor>; ct=60,
</message/warn>; title="POST warning texts here",
</message/info>; title="POST info texts here",
</log>; title="Most recent log messages"; if="tag:riot-os.org,2021:ser-out"

$ aiocoap-client coap://localhost/cbor
# CBOR message shown in Diagnostic Notation
{"hidden": false, "number": 32, "label": "Hello", "list": [1, 2, 3]}
```

The `/log` resource is rather hard to use manually, but there is a [tool for
it](https://pypi.org/project/coap-console/):

```sh
$ pipx run coap-console coap://localhost
INFO Server is ready.
```

This produces a continuous output of log activity as it happens; you can add entries from
another terminal using:

```sh
$ aiocoap-client coap://localhost/message/info -m POST --payload "This will be shown in a moment"
```

[coap-message]: https://crates.io/crates/coap-message
[aiocoap-client]: https://aiocoap.readthedocs.io/en/latest/installation.html

License: MIT OR Apache-2.0
