//! This module demonstrates the use of [coap_handler_implementations::TypeRenderable]

use heapless::{String, Vec};
use coap_handler_implementations::{
    new_dispatcher, HandlerBuilder, ReportingHandlerBuilder, TypeRenderable, TypeHandler
};
use coap_handler::{Attribute, Handler, Reporting};
use coap_numbers;
use core::default::Default;
use serde::{Deserialize, Serialize};

/// A demo struct with several members, all of which implement serde traits.
///
/// Using TypeRenderable, a CoAP GETter and SETter are implemented. To avoid being boring, both
/// do some checks: The setter rejects attempts to write HTML, and the getter refuses to answer if
/// the "hidden" property is true.
// Not going through minicbor because that has no implementations for heapless or vice versa
#[derive(Serialize, Deserialize, Clone)]
pub struct MyCBOR {
    hidden: bool,
    number: usize,
    label: String<32>,
    list: Vec<usize, 16>,
}

impl Default for MyCBOR {
    fn default() -> Self {
        Self {
            hidden: false,
            number: 32,
            label: "Hello".into(),
            list: Vec::from_slice(&[1, 2, 3])
                .expect("More than 3 entries allocated"),
        }
    }
}

impl TypeRenderable for MyCBOR {
    type Get = MyCBOR;
    type Put = MyCBOR;
    type Post = MyCBOR;

    fn get(&mut self) -> Result<MyCBOR, u8> {
        if self.hidden {
            return Err(coap_numbers::code::FORBIDDEN);
        }
        Ok(self.clone())
    }

    fn put(&mut self, new: &MyCBOR) -> u8 {
        if new.label.contains('<') {
            // No HTML injection please ;-)
            return coap_numbers::code::BAD_REQUEST;
        }
        *self = new.clone();
        coap_numbers::code::CHANGED
    }

    fn post(&mut self, _: &MyCBOR) -> u8 {
        coap_numbers::code::METHOD_NOT_ALLOWED
    }
}

/// Build a handler that gives access to a single [MyCBOR] object
///
/// This can be built with on no_std, and thus has no external synchronization -- the MyCBOR is
/// owned by the handler, and can only be accessed through the CoAP interface. For an alternative,
/// see [double_cbor_with_access]().
pub fn single_cbor_tree() -> impl Handler + Reporting {
    let cbor: MyCBOR = Default::default();

    new_dispatcher()
        .at_with_attributes(
            &["cbor"],
            &[Attribute::Ct(60)],
            TypeHandler::new(cbor),
            )
        .with_wkc()
}

/// Build a handler that gives access to a single [MyCBOR] object on two paths, and to the rest of
/// the application
///
/// As the MyCBOR object is now stored in an Arc and referenced through a Mutex, it can be in two
/// places in the tree at the same time, and even be accessed by the application at the same time.
#[cfg(feature = "std")]
pub fn double_cbor_with_access() -> (impl Handler + Reporting, std::sync::Arc<std::sync::Mutex<MyCBOR>>) {
    let cbor: std::sync::Arc<std::sync::Mutex<MyCBOR>> = Default::default();

    let handler = new_dispatcher()
        .at_with_attributes(
            &["cbor", "1"],
            &[],
            TypeHandler::new(TryingThroughMutex(cbor.clone())),
        )
        .at_with_attributes(
            &["cbor", "2"],
            &[],
            TypeHandler::new(TryingThroughMutex(cbor.clone())),
        )
        .with_wkc()
        ;

    (handler, cbor)
}

/// Helper struct that accesses a TypeRenderable intrough an `Arc<Mutex<_>>`, thus allowing easiy
/// simultaneous access.
///
/// When implementing SimpelCBORHandler, it does not wait for the lock, but rather fails with a
/// 5.03 Service Unavailable that usually prompts the client to retry. Note that this will not
/// happen ever if the items are just accessed through different paths on the same handler, and
/// neither will be if they are only ever locked outside the CoAP server's main loop. (And even
/// then, unless they're locked for long, it's very unlikely to be hit by chance).
///
/// TBD: Send a "Max-Age: 0" option along to indicate that the client can try again right away
/// rather than wait the usual 60 seconds.
///
/// TBD: This may be a nice addition to the [coap_handler] crate in general (but needs the
/// introduction of a `std` feature there).
#[cfg(feature = "std")]
pub struct TryingThroughMutex<T: TypeRenderable>(pub std::sync::Arc<std::sync::Mutex<T>>);

#[cfg(feature = "std")]
impl<T: TypeRenderable> TypeRenderable for TryingThroughMutex<T> {
    type Get = T::Get;
    type Put = T::Put;
    type Post = T::Post;

    fn get(&mut self) -> Result<Self::Get, u8> {
        self
            .0
            .try_lock()
            .map_err(|_| coap_numbers::code::SERVICE_UNAVAILABLE)?
            .get()
    }

    fn put(&mut self, new: &Self::Put) -> u8 {
        self.0
            .try_lock()
            .map(|mut s| s.put(new))
            .unwrap_or(coap_numbers::code::SERVICE_UNAVAILABLE)
    }

    fn post(&mut self, request: &Self::Post) -> u8 {
        self.0
            .try_lock()
            .map(|mut s| s.post(request))
            .unwrap_or(coap_numbers::code::SERVICE_UNAVAILABLE)
    }
}
