//! This module contains simple hello-world-ish handlers

use coap_handler_implementations::{new_dispatcher, HandlerBuilder, ReportingHandlerBuilder, SimpleRenderable, SimpleRendered};
use coap_handler::Handler;

/// By virtue of having a [SimpleRenderable] implementation, a static string will
/// already render as a plain text resource.
pub static WELCOME: SimpleRendered<&str> = SimpleRendered("Hello CoAP");

/// A stand-in for the current system time, implementing a simple text based clock when std is
/// enabled (and commenting on its no_stdness otherwise).
#[derive(Copy, Clone)]
pub struct Time;

pub static TIME: SimpleRendered<Time> = SimpleRendered(Time);

impl SimpleRenderable for Time {
    #[cfg(feature = "std")]
    fn render<W: core::fmt::Write>(&mut self, writer: &mut W) {
        write!(
            writer,
            "It's {} seconds past epoch.",
            std::time::SystemTime::now()
                .duration_since(std::time::SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs()
        )
        .unwrap();
    }

    #[cfg(not(feature = "std"))]
    fn render<W: core::fmt::Write>(&mut self, writer: &mut W) {
        write!(writer, "A no_std system knows no time.").unwrap();
    }

    fn content_format(&self) -> Option<u16> {
        Some(0 /* text/plain */)
    }
}

const POEM_TEXT: &str = "Aurea prima sata est aetas, quae vindice nullo,
sponte sua, sine lege fidem rectumque colebat.
Poena metusque aberant nec verba minantia fixo
aere legebantur, nec supplex turba timebat
iudicis ora sui, sed erant sine vindice tuti.
Nondum caesa suis, peregrinum ut viseret orbem,
montibus in liquidas pinus descenderat undas,
nullaque mortales praeter sua litora norant.
Nondum praecipites cingebant oppida fossae,
non tuba directi, non aeris cornua flexi,
non galeae, non ensis erant: sine militis usu
mollia securae peragebant otia gentes.

Ipsa quoque inmunis rastroque intacta nec ullis
saucia vomeribus per se dabat omnia tellus,
contentique cibis nullo cogente creatis
arbuteos fetus montanaque fraga legebant
cornaque et in duris haerentia mora rubetis
et quae deciderant patula Iovis arbore glandes.

Ver erat aeternum, placidique tepentibus auris
mulcebant zephyri natos sine semine flores;
mox etiam fruges tellus inarata ferebat,
nec renovatus ager gravidis canebat aristis;
flumina iam lactis, iam flumina nectaris ibant,
flavaque de viridi stillabant ilice mella.

Postquam Saturno tenebrosa in Tartara misso
sub Iove mundus erat, subiit argentea proles,
auro deterior, fulvo pretiosior aere.

Iuppiter antiqui contraxit tempora veris
perque hiemes aestusque et inaequalis autumnos
et breve ver spatiis exegit quattuor annum.
";

pub const POEM_TEXT_LEN: usize = POEM_TEXT.len();

#[derive(Copy, Clone)]
pub struct Poem;
impl coap_handler_implementations::SimpleRenderable for Poem {
    fn render<W: core::fmt::Write>(&mut self, writer: &mut W) {
        writer.write_str(POEM_TEXT).unwrap()
    }
}
pub static POEM: SimpleRendered<Poem> = SimpleRendered(Poem);

/// Build a handler that contains a hello-world text at `/`, and a `/time` resource
pub fn hello_world_tree() -> impl Handler {
    new_dispatcher()
        .at(&[], WELCOME)
        // Options can be used in many places; a None makes the resource Not Found and not
        // advertised.
        .at(&["time"], cfg!(feature = "std").then_some(TIME))
        .at(&["poem"], POEM)
        .with_wkc()
}
